<?php
namespace Kowal\CheckoutSortOrderShippingFields\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    const SECTIONS      = 'checkout_sort_order_shipping_fields';   // module name
    const GROUPS        = 'settings';        // setup general

    public function getConfig($cfg=null)
    {
        return $this->scopeConfig->getValue(
            $cfg,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
    
    public function getGeneralCfg($cfg=null,$store_id = 0)
    {
        $config = $this->scopeConfig->getValue(
            self::SECTIONS.'/'.self::GROUPS,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store_id
        );

        if(isset($config[$cfg])) return $config[$cfg];
        return $config;
    }

    public function getWebsiteCfg($cfg=null,$website_id = 0)
    {
        $config = $this->scopeConfig->getValue(
            self::SECTIONS.'/'.self::GROUPS,
            \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES,
            $website_id
        );

        if(isset($config[$cfg])) return $config[$cfg];
        return null;
    }

}
