<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\CheckoutSortOrderShippingFields\Plugin\Frontend\Magento\Checkout\Block\Checkout;

use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface;

class LayoutProcessor
{
    public function __construct(
        \Kowal\CheckoutSortOrderShippingFields\Helper\Data $dataHelper
    )
    {
        $this->dataHelper = $dataHelper;
    }

    public function aroundProcess(
        \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
        \Closure $proceed,
        $jsLayout
    )
    {
        //Your plugin code
        $result = $proceed($jsLayout);

        $this->storeManager = \Magento\Framework\App\ObjectManager::getInstance()->get(StoreManagerInterface::class);
        $store_id = 0;
        $store_id = $this->storeManager->getStore()->getId();
        $enable = $this->dataHelper->getGeneralCfg("enable", $store_id);
        if (!$enable) return $result;

        $fields = ['firstname', 'lastname', 'company', 'vat_id', 'street', 'postcode', 'city', 'telephone', 'region_id', 'country_id'];

        foreach ($fields as $field) {
            $result['components']['checkout']['children']['steps']['children']['shipping-step']
            ['children']['shippingAddress']['children']['shipping-address-fieldset']
            ['children'][$field]['sortOrder'] = $this->dataHelper->getGeneralCfg($field, $store_id);
        }

        return $result;
    }
}

